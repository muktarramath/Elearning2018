﻿using System.ComponentModel.DataAnnotations;

namespace Elearning2018prj.Models
{
    public class ChangeCredentials
    {
        [Required]
        public string Oldusername { get; set; }
        [Required]
        [StringLength(10)]
        public string Newusername { get; set; }
        [Required]
        [StringLength(10)]
        public string ConfirmNewusername { get; set; }
        [Required]
        public string Oldpassword { get; set; }
        [Required]
        [StringLength(10)]
        public string Newpassword { get; set; }
        [Required]
        [StringLength(10)]
        public string ConfirmNewpassword { get; set; }
    }
}