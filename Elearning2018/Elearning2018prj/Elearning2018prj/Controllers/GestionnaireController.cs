﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Elearning2018prj.Models;

namespace Elearning2018prj.Controllers
{
    public class GestionnaireController : Controller
    {
        ElearningDataContext db = new ElearningDataContext();
        
        public ActionResult Index()
        {
            return View();
        }


        // GET: Gestionnaire
        public ActionResult IndexStudent()
        {
            IEnumerable<Etudiant> students = db.Etudiants;
            return View(students);
        }
        

        // GET: Gestionnaire/Details/5
        public ActionResult DetailsStudent(int? id)
        {
            Etudiant query = db.Etudiants.FirstOrDefault(x => x.Idetudiant.Equals(id));

            return View(query);
        }

        // GET: Gestionnaire/Create
        public ActionResult CreateStudent()
        {
            return View();
        }

        // POST: Gestionnaire/Create
        [HttpPost]
        public ActionResult CreateStudent(Etudiant e)
        {
            try
            {
                // TODO: Add insert logic here

            Etudiant newStudent = new Etudiant
            {
                prenom = e.prenom,
                nom = e.nom,
                uname = e.uname,
                pwd = e.pwd,
                email = e.email,
                telephone = e.telephone
            };

                db.Etudiants.InsertOnSubmit(newStudent);
               db.SubmitChanges();

                return RedirectToAction("IndexStudent");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gestionnaire/Edit/5
        public ActionResult EditStudent(int? id)
        {
            Etudiant query = db.Etudiants.FirstOrDefault(x => x.Idetudiant.Equals(id));
            return View(query);
        }

        // POST: Gestionnaire/Edit/5
        [HttpPost]
        public ActionResult EditStudent(int? id, Etudiant e)
        {
            try
            {
                // TODO: Add update logic here

                Etudiant stud = db.Etudiants.FirstOrDefault(x => x.Idetudiant.Equals(id));

                stud.prenom = e.prenom;
                stud.nom = e.nom;
                stud.uname = e.uname;
                stud.pwd = e.pwd;
                stud.telephone = e.telephone;
                stud.email = e.email;

                db.SubmitChanges();

                return RedirectToAction("IndexStudent", "Gestionnaire");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gestionnaire/Delete/5
        public ActionResult DeleteStudent(int? id)
        {
            Etudiant etudiant = db.Etudiants.FirstOrDefault(x => x.Idetudiant.Equals(id));
            return View(etudiant);
        }

        // POST: Gestionnaire/Delete/5
        [HttpPost]
        public ActionResult DeleteStudent(int? id, Etudiant e)
        {
            
            try
            {
                // TODO: Add delete logic here
                Etudiant etudiant = db.Etudiants.FirstOrDefault(x => x.Idetudiant.Equals(id)&& x.nom.Equals(e.nom));

                if (etudiant != null)
                {
                    db.Etudiants.DeleteOnSubmit(etudiant);
                    db.SubmitChanges();


                    return RedirectToAction("Index", "Gestionnaire");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
        // ENSEIGNANT - TEACHER////////////////////////////////////////////////////

        // GET: Gestionnaire
        public ActionResult IndexTeacher()
        {
            
            IEnumerable<Enseignant> enseignants = db.Enseignants;

            return View(enseignants);
        }
        // GET: Gestionnaire/Details/5
        public ActionResult DetailsTeacher(int? id)
        {
            Enseignant query = db.Enseignants.FirstOrDefault(x => x.Idenseignant.Equals(id));
            return View(query);
        }

        // GET: Gestionnaire/Create
        public ActionResult CreateTeacher()
        {
            return View();
        }

        // POST: Gestionnaire/Create
        [HttpPost]
        public ActionResult CreateTeacher(Enseignant e)
        {

                Enseignant newTeacher = new Enseignant
                {
                    prenom = e.prenom,
                    nom = e.nom,
                    pwd = e.pwd,
                    email = e.email,
                    telephone = e.telephone,
                    admin = e.admin
                };

            try
            {
                // TODO: Add insert logic here

             
                db.Enseignants.InsertOnSubmit(newTeacher);
                
                db.SubmitChanges();

                return RedirectToAction("IndexTeacher");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gestionnaire/Edit/5
        public ActionResult EditTeacher(int? id)
        {
            Enseignant query = db.Enseignants.FirstOrDefault(x => x.Idenseignant.Equals(id));
            return View(query);
        }

        // POST: Gestionnaire/Edit/5
        [HttpPost]
        public ActionResult EditTeacher(int? id, Enseignant e)
        {
            try
            {
                // TODO: Add update logic here

                Enseignant t= db.Enseignants.FirstOrDefault(x => x.Idenseignant.Equals(id));

                t.prenom = e.prenom;
                t.nom = e.nom;
                t.pwd = e.pwd;
                t.telephone = e.telephone;
                t.email = e.email;
                t.admin = e.admin;

                db.SubmitChanges();

                return RedirectToAction("IndexTeacher", "Gestionnaire");
            }
            catch
            {
                return View();
            }
        }
        // GET: Gestionnaire/Delete/5
        public ActionResult DeleteTeacher(int? id)
        {
            Enseignant enseignant = db.Enseignants.FirstOrDefault(x => x.Idenseignant.Equals(id));
            return View(enseignant);
        }

        // POST: Gestionnaire/Delete/5
        [HttpPost]
        public ActionResult DeleteTeacher(int? id, Enseignant e)
        {

            try
            {
                // TODO: Add delete logic here
                Enseignant enseignant = db.Enseignants.FirstOrDefault(x => x.Idenseignant.Equals(id) && x.nom.Equals(e.nom));

                if (enseignant != null)
                {
                    db.Enseignants.DeleteOnSubmit(enseignant);
                    db.SubmitChanges();


                    return RedirectToAction("IndexTeacher", "Gestionnaire");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
        // Cours//////////////////////////////////////////////////////

        // GET: Gestionnaire
        public ActionResult IndexCour()
        {

           
            IEnumerable<Cour> cours = db.Cours;

            return View(cours);
        }
        // GET: Gestionnaire/Details/5
        public ActionResult DetailsCour(int? id)
        {
           Cour query = db.Cours.FirstOrDefault(x => x.Idcours.Equals(id));
            return View(query);
        }

        // GET: Gestionnaire/Create
        public ActionResult CreateCour()
        {
            return View();
        }

        // POST: Gestionnaire/Create
        [HttpPost]
        public ActionResult CreateCour(Cour c)
        {

            Cour newCour = new Cour
            {
                nomcours = c.nomcours,
                imagecours = c.imagecours,
                prixcours = c.prixcours,
                idenseignant = c.idenseignant
                
            };

            try
            {
                // TODO: Add insert logic here


                db.Cours.InsertOnSubmit(newCour);

                db.SubmitChanges();

                return RedirectToAction("IndexCour");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gestionnaire/Edit/5
        public ActionResult EditCour(int? id)
        {
             Cour query = db.Cours.FirstOrDefault(x => x.Idcours.Equals(id));
            return View(query);
        }

        // POST: Gestionnaire/Edit/5
        [HttpPost]
        public ActionResult EditCour(int? id, Cour c)
        {
            try
            {
                // TODO: Add update logic here

                Cour cc = db.Cours.FirstOrDefault(x => x.Idcours.Equals(id));

                cc.nomcours = c.nomcours;
                cc.imagecours = c.imagecours;
                cc.prixcours = c.prixcours;
                cc.idenseignant = c.idenseignant;

                db.SubmitChanges();

                return RedirectToAction("IndexCour", "Gestionnaire");
            }
            catch
            {
                return View();
            }
        }
        // GET: Gestionnaire/Delete/5
        public ActionResult DeleteCour(int? id)
        {
           Cour query = db.Cours.FirstOrDefault(x => x.Idcours.Equals(id));
            return View(query);
        }

        // POST: Gestionnaire/Delete/5
        [HttpPost]
        public ActionResult DeleteCour(int? id, Cour c)
        {

            try
            {
                // TODO: Add delete logic here
                Cour query = db.Cours.FirstOrDefault(x => x.Idcours.Equals(id)&& x.nomcours.Equals(c.nomcours));
                if (query != null)
                {
                    db.Cours.DeleteOnSubmit(query);
                    db.SubmitChanges();


                    return RedirectToAction("IndexCour", "Gestionnaire");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }

        // MODULE ////////////////////////////////////////////////////
             // GET: Gestionnaire
        public ActionResult IndexModule()
        {

            IEnumerable<Module> modules = db.Modules;

            return View(modules);
        }
        // GET: Gestionnaire/Details/5
        public ActionResult DetailsModule(int? id)
        {
            Module query = db.Modules.FirstOrDefault(x => x.Idmodule.Equals(id));
            return View(query);
        }

        // GET: Gestionnaire/Create
        public ActionResult CreateModule()
        {
            return View();
        }

        // POST: Gestionnaire/Create
        [HttpPost]
        public ActionResult CreateModule(Module m)
        {

            Module mm = new Module
            {
              nommodule = m.nommodule,
              video = m.video,
              exercise = m.exercise,
              quiz = m.quiz,
              solution = m.solution,
              labo = m.labo

            };

            try
            {
                // TODO: Add insert logic here


                db.Modules.InsertOnSubmit(mm);

                db.SubmitChanges();

                return RedirectToAction("IndexModule");
            }
            catch
            {
                return View();
            }
        }

        // GET: Gestionnaire/Edit/5
        public ActionResult EditModule(int? id)
        {
            Module query = db.Modules.FirstOrDefault(x => x.Idmodule.Equals(id));
            return View(query);
        }

        // POST: Gestionnaire/Edit/5
        [HttpPost]
        public ActionResult EditModule(int? id, Module m)
        {
            try
            {
                // TODO: Add update logic here


                Module mm = db.Modules.FirstOrDefault(x => x.Idmodule.Equals(id) && x.nommodule.Equals(m.nommodule));

                mm.nommodule = m.nommodule;
                mm.exercise = m.exercise;
                mm.labo = m.labo;
                mm.quiz = m.quiz;
                mm.solution = m.solution;
                mm.video = m.video;


                db.SubmitChanges();

                return RedirectToAction("IndexModule", "Gestionnaire");
            }
            catch
            {
                return View();
            }
        }
        // GET: Gestionnaire/Delete/5
        public ActionResult DeleteModule(int? id)
        {
           
            Module query = db.Modules.FirstOrDefault(x => x.Idmodule.Equals(id));
            return View(query);
        }

        // POST: Gestionnaire/Delete/5
        [HttpPost]
        public ActionResult DeleteModule(int? id, Module m)
        {

            try
            {
                // TODO: Add delete logic here

                Module query = db.Modules.FirstOrDefault(x => x.Idmodule.Equals(id) && x.nommodule.Equals(m.nommodule));
                if (query != null)
                {
                    db.Modules.DeleteOnSubmit(query);
                    db.SubmitChanges();


                    return RedirectToAction("IndexModule", "Gestionnaire");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
    }
}
