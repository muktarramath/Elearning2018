﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using System.Web.Mvc;
using Elearning2018prj.Models;
using WebGrease.Css.Ast.Selectors;

namespace Elearning2018prj.Controllers
{
    public class StudentController : Controller
    {
        ElearningDataContext db = new ElearningDataContext();
        // GET: Student
        public ActionResult Index()
        {
            Etudiant etudiant = TempData["student"] as Etudiant;

            
            List<Cour> listCours = (from p in db.Cours
                where !(from o in db.Inscriptions
                        where o.Idetudiant.Equals(etudiant.Idetudiant) 
                        select o.Idcours
                    )
                    .Contains(p.Idcours)
                select p).ToList();



            ViewBag.listCours = listCours;


            return View(etudiant);
        }

        // GET: Student/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Student/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Student/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Student/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Student/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
