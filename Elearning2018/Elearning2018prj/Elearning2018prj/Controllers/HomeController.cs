﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Web;
using System.Web.Mvc;

using Elearning2018prj.Models;

namespace Elearning2018prj.Controllers
{
    public class HomeController : Controller
    {
        ElearningDataContext db = new ElearningDataContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Register()
        {
           
            return View();
        }
        [HttpPost]
        public ActionResult Register(TempEtudiant temp)
        {
            if (VerifyEmail(temp.email))
            {
                Random r = new Random();

                string pwdgenerated = temp.telephone.Substring(0, 3) + (r.Next(7777, 9999)).ToString() + temp.prenom.Substring(0, 1);
                string aname = temp.prenom.Substring(0, 2) + temp.telephone.Substring(4, 2) + temp.nom.Substring(0, 2);

                TempEtudiant e = new TempEtudiant
                {
                    prenom = temp.prenom,
                    nom = temp.nom,
                    email = temp.email,
                    telephone = temp.telephone,
                    uname = aname,
                    pwd = pwdgenerated
                };
                db.TempEtudiants.InsertOnSubmit(e);
                db.SubmitChanges();

                return View("RegisterSuccessful", e);

            }
            else
            {
                ViewBag.Emailinfo = "Il y deja un account avec ce courriel ci";
                return View();
            }
          
        }

        public bool VerifyEmail(string email)
        {
            int query = db.Etudiants.Count(x => x.email.Equals(email));

            return (query == 0 ? true : false);
        }

        public ActionResult LoginStudent()
        {
         
            return View();
        }
        [HttpPost]
        public ActionResult LoginStudent(Etudiant e)
        {
            if (HasStudentinTempEtudiant(e))
            {
                //Redirect to Updatelogin information page.
               TempData["pwd"] = e.pwd;
               TempData["uname"] = e.uname;
                // return View("UpdateLoginStudent");
                return RedirectToAction("UpdateLoginStudent", "Home",e);

            }
            else
            {
                if (HasStudentinEtudiant(e))
                {
                    //TO redirect to Student login and create a variable session to Student Controller.
                    Etudiant stud = db.Etudiants.FirstOrDefault(x => x.uname.Equals(e.uname) && x.pwd.Equals(e.pwd));
                    TempData["student"] = stud;
                    
                    return RedirectToAction("Index", "Student", new { area = "" });
                }
                else
                {
                    ViewBag.MessageNonRegistered = "Impossible de trouver votre compte.";
                   
                    return View("Register");
                }


            }

            

        
        }

        private bool HasStudentinEtudiant(Etudiant e)
        {
            int q = db.Etudiants.Count(x => (x.uname.Equals(e.uname) && x.pwd.Equals(e.pwd)));

            return (q != 0);
        }

        private bool HasStudentinTempEtudiant(Etudiant e)
        {
            int q = db.TempEtudiants.Count(x => (x.uname.Equals(e.uname) && x.pwd.Equals(e.pwd)));
            return (q != 0);
        }
        
        public ActionResult UpdateLoginStudent()
        {

            ChangeCredentials c = new ChangeCredentials
            {
                Oldusername = TempData["uname"].ToString(),
                Newusername = "",
                ConfirmNewusername = "",
                Oldpassword = TempData["pwd"].ToString(),
                Newpassword = "",
                ConfirmNewpassword = ""
            };

            return View(c);
        }
        [HttpPost]
        public ActionResult UpdateLoginStudent(ChangeCredentials c)
        {
            if(!c.Oldusername.Equals(c.ConfirmNewusername) && !c.Oldpassword.Equals(c.ConfirmNewpassword))
            {
                TempEtudiant tmp = db.TempEtudiants.FirstOrDefault(x =>
                    (x.uname.Equals(c.Oldusername) && x.pwd.Equals(c.Oldpassword)));
                Etudiant t = new Etudiant
                {
                    prenom = tmp.prenom,
                    nom = tmp.nom,
                    uname = c.Newusername,
                    pwd = c.Newpassword,
                    telephone = tmp.telephone,
                    email = tmp.email

                };
               
                db.Etudiants.InsertOnSubmit(t);
                db.SubmitChanges();
             //TODO :DELETE the student record in TempEtudiant table.
                db.TempEtudiants.DeleteOnSubmit(tmp);
                db.SubmitChanges();

                return View("LoginStudent");
            }
            else
            {
                return View();
            }

        }

        public ActionResult LoginEnseignant()
        {
            return View();
        }
        
    }
}